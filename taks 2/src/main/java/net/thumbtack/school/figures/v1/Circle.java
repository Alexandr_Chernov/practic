package net.thumbtack.school.figures.v1;

import java.lang.Math;
import java.util.Objects;

public class Circle {

    private Point center;
    private int radius;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Circle circle = (Circle) o;
        return radius == circle.radius && Objects.equals(center, circle.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, radius);
    }

    public Circle(Point center, int radius){
        this.center = center;
        this.radius = radius;
    }
    public Circle(int xCenter, int yCenter, int radius){
        center = new Point(xCenter, yCenter);
        this.radius = radius;
    }
    public Circle(int radius){
        center = new Point(0, 0);
        this.radius = radius;
    }
    public Circle(){
        center = new Point(0, 0);
        this.radius = 1;
    }
    public Point getCenter(){
        return center;
    }
    public int getRadius(){
        return radius;
    }
    public void setCenter(Point center){
        this.center = center;
    }
    public void setRadius(int radius){
        this.radius = radius;
    }
    public void moveTo(int x, int y){
        center = new Point(x, y);
    }
    public void moveTo(Point point){
        center = point;
    }
    public void moveRel(int dx, int dy){
        center = new Point(center.getX() + dx, center.getY() + dy);
    }
    public void resize(double ratio){
        radius = (int)(radius * ratio);
    }
    public double getArea(){
        return Math.PI * Math.pow(radius, 2);
    }
    public double getPerimeter(){
        return 2 * Math.PI * radius;
    }
    public boolean isInside(int x, int y){
        return Math.pow(center.getX() - x, 2) + Math.pow(center.getY() - y, 2) <= Math.pow(radius, 2);
    }
    public boolean isInside(Point point){
        return Math.pow(center.getX() - point.getX(), 2) +
                Math.pow(center.getY() - point.getY(), 2) <= Math.pow(radius, 2);
    }
}