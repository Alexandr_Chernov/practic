package net.thumbtack.school.figures.v1;

import java.lang.Math;
import java.util.Objects;

public class Ellipse {

    private Point center;
    private int a, b;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ellipse ellipse = (Ellipse) o;
        return a == ellipse.a && b == ellipse.b && Objects.equals(center, ellipse.center);
    }

    @Override
    public int hashCode() {
        return Objects.hash(center, a, b);
    }

    public Ellipse(Point center, int xAxis, int yAxis){
        this.center = center;
        a = xAxis;
        b = yAxis;
    }
    public Ellipse(int xCenter, int yCenter, int xAxis, int yAxis){
        center = new Point(xCenter, yCenter);
        a = xAxis;
        b = yAxis;
    }
    public Ellipse(int xAxis, int yAxis){
        center = new Point(0, 0);
        a = xAxis;
        b = yAxis;
    }
    public Ellipse(){
        center = new Point(0, 0);
        a = 1;
        b = 1;
    }
    public Point getCenter(){
        return center;
    }
    public int getXAxis(){
        return a;
    }
    public int getYAxis(){
        return b;
    }
    public void setXAxis(int xAxis){
        a = xAxis;
    }
    public void setYAxis(int yAxis){
        b = yAxis;
    }
    public void setCenter(Point center){
        this.center = center;
    }
    public void moveTo(int x, int y){
        center = new Point(x, y);
    }
    public void moveTo(Point point){
        center = point;
    }
    public void moveRel(int dx, int dy){
        center = new Point(center.getX() + dx, center.getY() + dy);
    }
    public void resize(double ratio){
        a = (int)(a * ratio);
        b = (int)(b * ratio);
    }
    public void stretch(double xRatio, double yRatio){
        a = (int)(a * xRatio);
        b = (int)(b * yRatio);
    }
    public double getArea(){
        return Math.PI * a * b / 4;
    }
    public double getPerimeter(){
        return (double) 2 * Math.PI * Math.sqrt((Math.pow(a, 2) + Math.pow(b, 2)) / 8);
    }
    public boolean isInside(int x, int y){
        return Math.pow(x - center.getX(), 2) / Math.pow((double) a / 2, 2) +
                Math.pow(y - center.getY(), 2) / Math.pow((double) b / 2, 2) <= 1;
    }
    public boolean isInside(Point point){
        return Math.pow(point.getX() - center.getX(), 2) / Math.pow((double) a / 2, 2) +
                Math.pow(point.getY() - center.getY(), 2) / Math.pow((double) b / 2, 2) <= 1;
    }
}
