package net.thumbtack.school.figures.v1;

import java.lang.Math;
import java.util.Objects;

public class Rectangle {

    private Point leftTop, rightBottom;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rectangle rectangle = (Rectangle) o;
        return Objects.equals(leftTop, rectangle.leftTop) && Objects.equals(rightBottom, rectangle.rightBottom);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftTop, rightBottom);
    }

    public Rectangle(Point leftTop, Point rightBottom){
        this.leftTop = leftTop;
        this.rightBottom = rightBottom;
    }
    public Rectangle(int xLeft, int yTop, int xRight, int yBottom){
        leftTop = new Point(xLeft, yTop);
        rightBottom = new Point(xRight, yBottom);
    }
    public Rectangle(int length, int width){
        leftTop = new Point(0,-width);
        rightBottom = new Point(length, 0);
    }
    public Rectangle(){
        leftTop = new Point(0, -1);
        rightBottom = new Point(1, 0);
    }
    public Point getTopLeft(){
        return leftTop;
    }
    public Point getBottomRight(){
        return rightBottom;
    }
    public void setTopLeft(Point topLeft){
        leftTop = topLeft;
    }
    public void setBottomRight(Point bottomRight){
        rightBottom = bottomRight;
    }
    public int getLength(){
        return Math.abs(leftTop.getX() - rightBottom.getX());
    }
    public int getWidth(){
        return Math.abs(leftTop.getY() - rightBottom.getY());
    }
    public void moveTo(int x, int y){
        rightBottom = new Point(x + getLength(), y + getWidth());
        leftTop = new Point(x, y );
    }
    public void moveTo(Point point){
        rightBottom = new Point(point.getX() + getLength(), point.getY() + getWidth());
        leftTop = new Point(point.getX(), point.getY());
    }
    public void moveRel(int dx, int dy){
        leftTop = new Point(leftTop.getX() + dx, leftTop.getY() + dy);
        rightBottom = new Point(rightBottom.getX() + dx, rightBottom.getY() + dy);
    }
    public void resize(double ratio){
        rightBottom = new Point((int)(leftTop.getX() + getLength() * ratio),
                (int)(leftTop.getY() + getWidth() * ratio));
    }
    public void stretch(double xRatio, double yRatio){
        rightBottom = new Point((int)(leftTop.getX() + getLength() * xRatio),
                (int)(leftTop.getY() + getWidth() * yRatio));
    }
    public double getArea(){
        return getLength() * getWidth();
    }
    public double getPerimeter(){
        return getLength() * 2 + getWidth() * 2;
    }
    public boolean isInside(int x, int y){
        return x >= leftTop.getX() && x <= rightBottom.getX()
                && y >= leftTop.getY() && y <= rightBottom.getY();
    }
    public boolean isInside(Point point){
        return point.getX() >= leftTop.getX() && point.getX() <= rightBottom.getX()
                && point.getY() >= leftTop.getY() && point.getY() <= rightBottom.getY();
    }
    public boolean isIntersects(Rectangle rectangle){
        if ((rectangle.getTopLeft().getX() >= leftTop.getX() &&
                rectangle.getTopLeft().getX() <= rightBottom.getX() ||
                rectangle.getBottomRight().getX() >= leftTop.getX() &&
                        rectangle.getBottomRight().getX() <= rightBottom.getX()) &&
                ((rectangle.getTopLeft().getY() >= leftTop.getY() &&
                        rectangle.getTopLeft().getY() <= rightBottom.getY()) ||
                        (rectangle.getBottomRight().getY() >= leftTop.getY() &&
                                rectangle.getBottomRight().getY() <= rightBottom.getY()))){
            return true;
        }
        return (leftTop.getX() >= rectangle.getTopLeft().getX() &&
                leftTop.getX() <= rectangle.getBottomRight().getX() ||
                rightBottom.getX() >= rectangle.getTopLeft().getX() &&
                        rightBottom.getX() <= rectangle.getBottomRight().getX()) &&
                (leftTop.getY() >= rectangle.getTopLeft().getY() &&
                        leftTop.getY() <= rectangle.getBottomRight().getY() ||
                        rightBottom.getY() >= rectangle.getTopLeft().getY() &&
                        rightBottom.getY() <= rectangle.getBottomRight().getY());
    }
    public boolean isInside(Rectangle rectangle){
        return rectangle.getTopLeft().getX() > leftTop.getX() &&
                rectangle.getTopLeft().getX() < rightBottom.getX() &&
                rectangle.getTopLeft().getY() > leftTop.getY() &&
                rectangle.getTopLeft().getY() < rightBottom.getY() &&
                rectangle.getBottomRight().getX() > leftTop.getX() &&
                rectangle.getBottomRight().getX() < rightBottom.getX() &&
                rectangle.getBottomRight().getY() > leftTop.getY() &&
                rectangle.getBottomRight().getY() < rightBottom.getY();
    }
}
