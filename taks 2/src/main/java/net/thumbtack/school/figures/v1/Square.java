package net.thumbtack.school.figures.v1;

import java.util.Objects;

public class Square {

    private Point leftTop;
    private int size;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Square square = (Square) o;
        return size == square.size && Objects.equals(leftTop, square.leftTop);
    }

    @Override
    public int hashCode() {
        return Objects.hash(leftTop, size);
    }

    public Square(Point leftTop, int size){
        this.leftTop = leftTop;
        this.size = size;
    }
    public Square(int xLeft, int yTop, int size){
        leftTop = new Point(xLeft, yTop);
        this.size = size;
    }
    public Square(int size){
        leftTop = new Point(0, -size);
        this.size = size;
    }
    public Square(){
        leftTop = new Point(0, -1);
        size = 1;
    }
    public Point getTopLeft(){
        return leftTop;
    }
    public Point getBottomRight(){
        return new Point(leftTop.getX() + size, leftTop.getY() + size);
    }
    public void setTopLeft(Point topLeft){
        leftTop = topLeft;
    }
    public int getLength(){
        return size;
    }
    public void moveTo(int x, int y){
        leftTop = new Point(x, y);
    }
    public void moveTo(Point point){
        leftTop = point;
    }
    public void moveRel(int dx, int dy){
        leftTop = new Point(leftTop.getX() + dx, leftTop.getY() + dy);
    }
    public void resize(double ratio){
        size = (int)(size * ratio);
    }
    public double getArea(){
        return size * size;
    }
    public double getPerimeter(){
        return size * 4;
    }
    public boolean isInside(int x, int y){
        return x >= leftTop.getX() && x <= leftTop.getX() + size &&
                y >= leftTop.getY() && y <= leftTop.getY() + size;
    }
    public boolean isInside(Point point){
        return point.getX() >= leftTop.getX() && point.getX() <= leftTop.getX() + size &&
                point.getY() >= leftTop.getY() && point.getY() <= leftTop.getY() + size;
    }
    public boolean isIntersects(Square square){
        if ((square.getTopLeft().getX() >= leftTop.getX() &&
                square.getTopLeft().getX() <= getBottomRight().getX() ||
                square.getBottomRight().getX() >= leftTop.getX() &&
                        square.getBottomRight().getX() <= getBottomRight().getX()) &&
                ((square.getTopLeft().getY() >= leftTop.getY() &&
                        square.getTopLeft().getY() <= getBottomRight().getY()) ||
                        (square.getBottomRight().getY() >= leftTop.getY() &&
                                square.getBottomRight().getY() <= getBottomRight().getY()))){
            return true;
        }
        return (leftTop.getX() >= square.getTopLeft().getX() &&
                leftTop.getX() <= square.getBottomRight().getX() ||
                getBottomRight().getX() >= square.getTopLeft().getX() &&
                        getBottomRight().getX() <= square.getBottomRight().getX()) &&
                (leftTop.getY() >= square.getTopLeft().getY() &&
                        leftTop.getY() <= square.getBottomRight().getY() ||
                        getBottomRight().getY() >= square.getTopLeft().getY() &&
                                getBottomRight().getY() <= square.getBottomRight().getY());
    }
    public boolean isInside(Square square){
        return square.getTopLeft().getX() >= leftTop.getX() &&
                square.getTopLeft().getX() <= getBottomRight().getX() &&
                square.getTopLeft().getY() >= leftTop.getY() &&
                square.getTopLeft().getY() <= getBottomRight().getY() &&
                square.getBottomRight().getX() >= leftTop.getX() &&
                square.getBottomRight().getX() <= getBottomRight().getX() &&
                square.getBottomRight().getY() >= leftTop.getY() &&
                square.getBottomRight().getY() <= getBottomRight().getY();
    }
}
